from sqlalchemy import *
 
global_table_list = (
    ('t_table1', (
        Column('c_id',Integer,Sequence('t_table1_id_seq'),primary_key=True),  
        Column('c_name', String(40)),  
        Column('c_keywords', Text),  
        Column('c_time', TIMESTAMP),
        Column('c_status', Integer),
        Column('c_comment', String(20)),                
        )
    ),
    ('t_table2', (
        Column('c_id',Integer,Sequence('t_table2_id_seq'),primary_key=True),  
        Column('c_type', Integer),
        Column('c_sex', BOOLEAN),
        Column('c_province', Integer),
        Column('c_area', Integer),
        Column('c_platform', String(40)),
        Column('c_url', String(100)),
        )
    ),
    ('t_table3', (
        Column('c_rule_id',Integer),  
        Column('c_keyword', String(40)),  
        Column('c_timeline', String(20)),             
        )
    ),
)
 
def init_db():
    db = create_engine('sqlite:///db//test.db')
 
    db.echo = False  # Try changing this to True and see what happens
 
    metadata = MetaData(db)
 
    for (t_name,t_columns) in global_table_list:
        try:
            cur_table = Table(t_name,metadata,autoload=True)
        except:
            #下面这句等价于：cur_table = Table(t_name,metadata,t_columns)
            #apply表示调用函数，这里是调用函数Table()
            #apply的调用形式如下，apply(func_name,args),apply的第二个参数args要是列表的形式
            #所以，这里args是个参数列表，要以(arg1,arg2,...)的形式调用
            
            cur_table = Table(t_name,metadata,*t_columns)
            cur_table.create()
 
if __name__ == '__main__':
    init_db()